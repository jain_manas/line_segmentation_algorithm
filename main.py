import cv2
if __name__ == "__main__":
    # read image
    img = cv2.imread("test/X51005288570.jpg", cv2.IMREAD_UNCHANGED)
    print(f"The shape of original image is {img.shape}")
    image = cv2.imread("test/X51005288570.jpg",0)
    img_gray = cv2.imread("test/X51005288570.jpg", cv2.IMREAD_GRAYSCALE)
    # thresh = 127
    # im_bw = cv2.threshold(img_gray, thresh, 255, cv2.THRESH_BINARY)[1]
    (thresh, im_bw) = cv2.threshold(img_gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    n = im_bw.shape[0]
    m = im_bw.shape[1]
    
    # im_bw has value either (255 = white) or (0 = black)
    hyper_parameter_for_width = 0.001*m
    
    count_width=0
    rows_imp = []
    for i in range(n):
        for j in range(m):
            if count_width>=hyper_parameter_for_width:
                count_width=0
                rows_imp.append(i)
                break
            if im_bw[i][j]==0:
                count_width+=1

    final_imgs_list = []
    start_ind = 0
    end_ind = 1
    for i in range(1,len(rows_imp)):
        if rows_imp[i]==rows_imp[i-1]+1:
            end_ind = i
        else:
            final_imgs_list.append(rows_imp[start_ind:i])
            start_ind = i

    padding = 4
    output_path = "result"
    croppings = []
    for i in range(len(final_imgs_list)):
        print(f"i = {i}")
        # To skip image crops having very small height (mainly have ----- or **** (no textual information))
        if len(final_imgs_list[i])<=10:
            continue
        # to skip image crops having more than 1 sentences (mainly occurs if there is a table in the pdf or some handwritten signatures)
        if len(final_imgs_list[i])>=100:
            continue
        start_row = final_imgs_list[i][0]
        end_row = final_imgs_list[i][-1]
        
        im_bw_temp = im_bw[start_row:end_row+1][:]
        
        height_of_croping = im_bw_temp.shape[0]

        count_height = 0
        hyper_parameter_for_height = 0.001*height_of_croping
        cols_imp = []
        for p in range(im_bw_temp.shape[1]):
            for q in range(im_bw_temp.shape[0]):
                if count_height>=hyper_parameter_for_height:
                    count_height=0
                    cols_imp.append(p)
                    break
                if im_bw_temp[q][p]==0:
                    count_height+=1

        left_most = cols_imp[0]
        right_most = cols_imp[-1]

        horizontal_padding = 8
        im_bw_final = img[start_row-padding:end_row+1+padding,left_most-horizontal_padding:right_most+1+horizontal_padding,:]
        print(f"The shape of image_{i} is : {im_bw_final.shape}")
        cv2.imwrite(f"./result/img_{i}.png", im_bw_final)

