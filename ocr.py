from PIL import Image
from PIL import ImageFile
import pytesseract
import os

path = "./result/"
files = os.listdir(path)
print(files)

# cmd = f"ls {imgs_dir}"
# files = subprocess.check_output(cmd, shell=True).decode('utf-8').strip().split('\n')
#     print("Files to be OCRed:")
#     print(*files,sep="\n")

#     imgs = [x.split('/')[-1] for x in files[1:] if x.split('/')]
text = ""
for f in files:
    custom_oem_psm_config = r'--psm 7 --dpi 300'
    txt=pytesseract.image_to_string(Image.open(f"./result/{f}"),config=custom_oem_psm_config)
    print(txt)
    text += txt
    # text += "\n"

with open(f"./output.txt", "w", encoding="utf-8") as f:
    f.write(text)


# custom_oem_psm_config = r'--psm 7 --dpi 300'
complete_img_text =pytesseract.image_to_string(Image.open("./test/X51005288570.jpg"))
with open(f"./complete_img_ocr_output.txt", "w", encoding="utf-8") as f:
    f.write(complete_img_text)